package ru.gromov.outstreamtest.contracts.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import ru.gromov.outstreamtest.contracts.entity.ClientInfo;
import ru.gromov.outstreamtest.contracts.generator.ContractGenerator;

import java.io.File;
import java.io.FileInputStream;

@Service
@AllArgsConstructor
public class ContractService {
    private final ContractGenerator contractGenerator;

    public InputStreamResource getContract(ClientInfo clientInfo) {
        File contract = contractGenerator.generateContract(clientInfo);

        return getISResource(contract);
    }

    @SneakyThrows
    private InputStreamResource getISResource(File file) {
        return new InputStreamResource(new FileInputStream(file));
    }

}
