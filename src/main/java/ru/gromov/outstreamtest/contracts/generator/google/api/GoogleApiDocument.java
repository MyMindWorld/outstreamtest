package ru.gromov.outstreamtest.contracts.generator.google.api;

import lombok.Data;


@Data
public class GoogleApiDocument {
    private long id;
}
