package ru.gromov.outstreamtest.contracts.generator.google;

import lombok.AllArgsConstructor;
import ru.gromov.outstreamtest.contracts.generator.ContractGenerator;
import ru.gromov.outstreamtest.contracts.generator.google.api.GoogleApiClient;
import ru.gromov.outstreamtest.contracts.generator.google.api.GoogleApiDocument;
import ru.gromov.outstreamtest.contracts.generator.google.config.GoogleTemplateConfig;
import ru.gromov.outstreamtest.contracts.entity.ClientInfo;

import java.io.File;


@AllArgsConstructor
public class GoogleContractGenerator implements ContractGenerator {
    private final GoogleTemplateConfig templateConfig;
    private final GoogleApiClient googleApiClient;

    @Override
    public File generateContract(ClientInfo clientInfo) {
        GoogleApiDocument clonedTemplate = googleApiClient.cloneFile(templateConfig.getTemplateId());

        modifyTemplateFields(clientInfo, clonedTemplate);

        File generatedContract = googleApiClient.downloadFile(clonedTemplate.getId());

        googleApiClient.deleteFile(clonedTemplate.getId());

        return generatedContract;
    }

    private void modifyTemplateFields(ClientInfo clientInfo, GoogleApiDocument clonedTemplate) {
        // В случае разработки реального сервиса, эту часть я бы оптимизировал
        // И добавил маппинг между полями дата класса клиента и темплейта
        googleApiClient.modifyFile(clonedTemplate.getId(), "{client_name}", clientInfo.getName());
        googleApiClient.modifyFile(clonedTemplate.getId(), "{client_company}", clientInfo.getCompany());
    }
}
