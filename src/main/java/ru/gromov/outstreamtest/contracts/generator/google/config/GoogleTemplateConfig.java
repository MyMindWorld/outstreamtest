package ru.gromov.outstreamtest.contracts.generator.google.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "google-template-generator")
@Getter
@Setter
public class GoogleTemplateConfig {
    /**
     * Id of contract template on google api.
     */
    private long templateId;
}
