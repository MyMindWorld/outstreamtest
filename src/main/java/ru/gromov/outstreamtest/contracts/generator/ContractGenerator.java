package ru.gromov.outstreamtest.contracts.generator;

import ru.gromov.outstreamtest.contracts.entity.ClientInfo;

import java.io.File;

public interface ContractGenerator {
    File generateContract(ClientInfo clientInfo);
}
