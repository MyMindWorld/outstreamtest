package ru.gromov.outstreamtest.contracts.generator.google.api;

import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class GoogleApiClient {
    public GoogleApiDocument cloneFile(long id) {
        return new GoogleApiDocument();
    }

    public void modifyFile(long id, String field, String value) { }

    public void deleteFile(long id) {
    }

    public File downloadFile(long id) {
        return new File("");
    }
}
