package ru.gromov.outstreamtest.contracts.generator;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.gromov.outstreamtest.contracts.generator.google.GoogleContractGenerator;
import ru.gromov.outstreamtest.contracts.generator.google.api.GoogleApiClient;
import ru.gromov.outstreamtest.contracts.generator.google.config.GoogleTemplateConfig;

@Configuration
@EnableConfigurationProperties(GoogleTemplateConfig.class)
public class ContractGeneratorConfig {

    @Bean(name = "googleTemplateGenerator")
    @ConditionalOnProperty(prefix = "contract-generator", name = "service", havingValue = "google")
    public ContractGenerator contractTemplateProvider(GoogleTemplateConfig googleTemplateConfig, GoogleApiClient googleApiClient) {
        return new GoogleContractGenerator(googleTemplateConfig, googleApiClient);
    }
}
