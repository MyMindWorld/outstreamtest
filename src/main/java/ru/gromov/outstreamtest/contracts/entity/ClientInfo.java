package ru.gromov.outstreamtest.contracts.entity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ClientInfo {
    @NotBlank(message = "Client name cannot be empty")
    private String name;
    @NotBlank(message = "Client company cannot be empty")
    private String company;
}
