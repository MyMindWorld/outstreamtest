package ru.gromov.outstreamtest.contracts.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.gromov.outstreamtest.contracts.service.ContractService;
import ru.gromov.outstreamtest.contracts.entity.ClientInfo;

@RequiredArgsConstructor
@RestController("/api/contracts")
public class ContractController {
    private final ContractService contractService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    @Operation(summary = "Generate contract for client")
    public ResponseEntity<InputStreamResource> generateContract(@RequestBody ClientInfo clientInfo) {
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(contractService.getContract(clientInfo));
    }
}
