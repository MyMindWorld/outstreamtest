package ru.gromov.outstreamtest.contracts.generator.google;

import org.junit.Test;

import static org.junit.Assert.*;

public class GoogleContractGeneratorTest {

    public static class CheckThatContractGenerationCalls {

        @Test
        public void cloningForTemplate() {
        }

        @Test
        public void modificationForEachField() {
        }

        @Test
        public void downloadingOfFile() {
        }

        @Test
        public void duplicateRemoval() {
        }
    }

    public static class CheckThatContractGenerationDoesNotCalls {

        @Test
        public void anythingButCloningWithTemplateId() {
        }

        @Test
        public void modifyingFieldsWithCorrectMapping() {
        }

        @Test
        public void downloadingOfFile() {
        }

        @Test
        public void duplicateRemoval() {
        }
    }
}